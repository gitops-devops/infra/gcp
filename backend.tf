terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "myorganization1"

    workspaces {
      name = "gcp"
    }
  }
}
